package com.example.CountriesProject.adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.example.CountriesProject.R;
import com.example.CountriesProject.Entidades.Pais;
import android.webkit.*;
import java.util.List;

public class AdapterPais extends RecyclerView.Adapter<AdapterPais.MyViewHolder>{

    public List<Pais>listapaises;

    public AdapterPais(List<Pais> paises){

        this.listapaises = paises;
    }


    @NonNull
    @Override
    public AdapterPais.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lista_banderas,parent, false);


        return new AdapterPais.MyViewHolder(v);


    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position){

         Pais pais=listapaises.get(position);
         holder.pais.setText(pais.getNombre());
         holder.capital.setText(pais.getCapital());
         Glide.with(holder.itemView.getContext()).load(pais.getUrlBandera()).into(holder.image);
        holder.webHimno.loadData(listapaises.get(position).getUrlHimno(), "text/html","utf-8");

    }

    @Override
    public int getItemCount(){
        return listapaises.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{


        public TextView pais,capital;
        public ImageView image;
        public WebView webHimno;

        public MyViewHolder(View view){
            super(view);
            pais= view.findViewById(R.id.Nombre);
            image=view.findViewById(R.id.Imagen_1);
            capital=view.findViewById(R.id.Capital);
            webHimno=(WebView) view.findViewById(R.id.video);
            webHimno.getSettings().setJavaScriptEnabled(true);
            webHimno.setWebChromeClient(new WebChromeClient());

        }
    }


}
